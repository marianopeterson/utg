# UTG: Unix to Gregorian converter

Provides a web page that converts time between multiple formats. It helps convert between Unix timestamps and Gregorian dates. It also understands relative times ("3pm tomorrow") by leveraging PHP's [strtotime()](http://php.net/strtotime) function.


## Setup

This service uses nginx and php, and runs on Docker. The service can be launched as a set of orchestrated containers using `docker-compose`, or as a single container using `docker run`.

### Multiple Containers
The multi-container approach is orchestrated using Docker Compose, and defined in [docker-compose.yml](docker-compose.yml). This approach creates one container to run the web server (Nginx), and a second container to run the CGI gateway and application code (PHP-FPM). These containers are based on the [nginx:latest](https://hub.docker.com/_/nginx) and [php:7-fpm](https://hub.docker.com/_/php/?tab=tags&name=7-fpm) images from the [Docker Hub](https://hub.docker.com/) image repo. Docker Compose also creates a network for the containers to communicate with each other, and uses DNS to make the containers addressable within the network. We've named the containers "nginx.utg" and "php.utg" in the services section of [docker-compose.yml](docker-compose.yml). We need to make the containers addressable by static names because we don't know what IP addresses each container will be assigned at runtime, but we need a name by which to reference them in configuration files. For example, in the Nginx configuration we need a way to specify the PHP container to which HTTP requests will be forwarded. We achieve this by referencing the PHP container's name (e.g., `fastcgi_pass php:9000`) instead of an IP address (e.g., `fastcgi_pass 127.0.0.1:9000`).

Use `docker-compose` to launch the service as multiple containers:

    docker-compose up -d

We can check that the service is working, and SSH into the containers:

    curl localhost:8080/tomorrow
    docker exec -it nginx.utg /bin/sh
    docker exec -it php.utg /bin/sh
    docker logs -f nginx.utg

To call it from the command line:

    docker exec -it php.utg php /code/utg.php "tomorrow 3pm"
    docker exec -e UTG_TZ=America/Los_Angeles -it php.utg php /code/utg.php "tomorrow 3pm"

We can also stop our containers and cleanup the network:

    docker-compose down

### Single Container
Alternatively, we can launch this service as a single container using `docker run`. In this case our container is based on the [trafex/alpine-nginx-php7](https://hub.docker.com/r/trafex/alpine-nginx-php7/) image from the [Docker Hub](https://hub.docker.com/) image repo. In order for our Nginx configuration to work we need to tell Docker to setup the DNS names that we used in the Nginx configuration (e.g., "php"). We achieve this by first creating a Docker network which establishes a DNS namespace. Then we launch the container within that network, and set its DNS name to match the one we used in the Nginx configuration.

Use `docker run` to launch the service as a single container:

    docker network create --driver bridge utg-network
    docker run -d \
               -v `pwd`/code:/var/www/html \
               -v `pwd`/nginx.conf:/etc/nginx/conf.d/server.conf \
               -p 8080:8080 \
               --network utg-network \
               --name php \
               trafex/alpine-nginx-php7

We can check that the service is working, and SSH into the containers:

    curl localhost:8080/tomorrow
    docker exec -it php /bin/sh
    docker logs -f php

We can also stop our container and cleanup the network:

    docker stop php
    docker rm php
    docker network prune

# Reference

- [Making your dockerised PHP application even better - The Geeky Platypus Blog](http://geekyplatypus.com/making-your-dockerised-php-application-even-better/)
- [Overview of Docker Networks](https://docs.docker.com/network/)
- [Networking with standalone containers - Docker Docs](https://docs.docker.com/network/network-tutorial-standalone/)
- [Networking with Docker Compose - Docker Docs](https://docs.docker.com/compose/networking/)
- [Inspect an image - Microbadger](https://microbadger.com/images/trafex/alpine-nginx-php7)
