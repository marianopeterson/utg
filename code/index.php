<?php
function autocorrect($input): string {
    // problem:  strtotime can't parse 'tomorrow at 3pm'
    // solution: replace "tomorrow at 3pm" with "tomorrow 3pm"
    //           and:    "tomorrow at noon" with "tomorrow noon"
    // note:     "AT" is a valid timezone (Atlantic Time)
    $input = preg_replace('/ at (\d|noon)/i', ' ${1}', $input);

    // problem:  strtotime can't parse '2023-07-09 00:00:00 PDT (-0700)'
    //           Time offsets that are wrapped in parentheses break the parser.
    // solution: replace "(-7000)" with "-7000"
    return preg_replace("/\(([-+]\d+)\)/i", '${1}', $input);
}

function timestamp($input): int|bool {
    if ($input == "") {
        $timestamp = time();
    } else if (is_numeric($input)) {
        $timestamp = $input; // assume input provided as unix timestamp
    } else {
        $timestamp = strtotime($input);
    }
    return $timestamp;
}

function render($file, $vars): void {
    extract($vars);
    include dirname(__FILE__) . '/' . $file;
}

$timezone = $_COOKIE['timezone'] ?? 'America/Los_Angeles';
date_default_timezone_set($timezone);

$input = trim(trim(rawurldecode($_SERVER['REQUEST_URI'])), "/");
$input = autocorrect($input);
$timestamp = timestamp($input);
$sanitized_input = htmlentities($input); // for rendering back to the user

$error = 0;
if ($timestamp === false) {  // strtotime could not parse $input
    $error = 1;
    $timestamp = 0;
}

render('index.tpl', array(
    'error' => $error,
    'sanitized_input' => $sanitized_input,
    'timestamp' => $timestamp,
    'timezone' => $timezone,
));
