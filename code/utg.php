#!/usr/bin/env php
<?php
function usage() {
    print <<<EOT

ABOUT
    Prints the unix timestamp and textual time representation
    for a given time value. The given time can be provided as
    a Unix timestamp or a textual time representation.

USAGE
    utg [-t|--timezone=TIMEZONE] [unix timestamp or time as text]

OPTIONS
    -h|--help       Print usage information.
    -t|--timezone   Set timezone used for relative dates and output.
                    ex. "America/Los_Angeles", "PST"

                    Timezone can also be set via the UTG_TZ variable.
                    ex. export UTG_TZ="America/Los_Angeles"

                    Note: if this script is running in a docker container,
                    it will not inherit the environment variables set
                    outside the container by the docker host. 

                    Note: TZ is not used because it's a standard environment
                    variable: https://www.gnu.org/software/libc/manual/html_node/Standard-Environment.html

EXAMPLE
    utg 1283392872
    utg "midnight"
    utg "today"
    utg "3 pm"
    utg "2 days ago"
    utg --timezone=PST "tomorrow""

EOT;
    exit(0);
}

$pos_args_idx = 1; // index of argv where positional arguments begin (after -t VAL)
$options = getopt('ht:', ['help', 'timezone:'], $pos_args_idx);
if (isset($options['h']) or isset($options['help'])) {
    usage();
}

$timezone = $options['timezone']
    ?? $options['t']
    ?? $_ENV['UTG_TZ']
    ?? 'America/Los_Angeles';
$abbrevs = DateTimeZone::listAbbreviations();
if (isset($abbrevs[strtolower($timezone)])) {
    // Converts 'est' to 'America/New_York', which has the benefit
    // of correcting for daylight savings (est vs edt).
    $timezone = $abbrevs[strtolower($timezone)][0]['timezone_id'];
} else {
    $timezone = mb_convert_case($timezone, MB_CASE_TITLE);
    if (!in_array($timezone, DateTimeZone::listIdentifiers())) {
        print "Unrecognized timezone: $timezone\n";
        exit(1);
    }
}
date_default_timezone_set($timezone);

if (isset($argv[$pos_args_idx])) {
    if (is_numeric($argv[$pos_args_idx])) {
        $time = $argv[$pos_args_idx];
    } else {
        $time = strtotime($argv[$pos_args_idx]);
    }
} else {
    $time = time();
}
print "Unix time: $time\n";
print date('D, M j, Y g:i:s A T (O)', $time) ."\n";
