<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>UTG: Unix to Gregorian</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/css/bootstrap.min.css" integrity="sha512-t4GWSVZO1eC8BM339Xd7Uphw5s17a86tIZIj8qRxhnKub6WoyhnrxeCIMeAqBPgdZGlCcG2PrZjMc+Wr78+5Xg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .text-underline-hover {
            text-decoration: none;
        }

        .text-underline-hover:hover {
            text-decoration: underline;
        }
    </style>
    <script>
        unix_timestamp = <?= $timestamp ?>;
    </script>
</head>
<body>

<div class="container">

    <div class="my-3 p-5 text-start bg-body-tertiary border rounded-3">
        <h1 class="text-body-emphasis">UTG: Unix to Gregorian</h1>
        <p class="lead">Parse a datetime, including <a href="https://www.php.net/manual/en/datetime.formats.relative.php" class="text-underline-hover">relative formats</a> such as "tomorrow 8am", into a Unix timestamp and Gregorian date. Leverages PHP's <a href="http://php.net/strtotime" class="text-underline-hover">strtotime()</a> function.</p>
    </div>

    <?php if ($error): ?>
        <?php include('error.tpl') ?>
    <?php endif ?>

    <form id="input">
        <table class="table table-bordered">
            <tr>
                <th class="col-md-1">Input</th>
                <td class="col-md-4"><input type="text" name="datetime" id="datetime" value="<?= $sanitized_input ?>" class="form-control" autofocus></td>
            </tr>
            <tr>
                <th>Unix timestamp</th>
                <td id="timestamp_style_date"></td>
            </tr>
            <tr>
                <th>Gregorian date</th>
                <td id="gregorian_style_date"></td>
            </tr>
            <tr>
                <th>Log style date</th>
                <td id="log_style_date"></td>
            </tr>
            <tr>
                <th>Relative to now</th>
                <td id="duration_style_date"></td>
            </tr>
        </table>

        <button type="submit" class="btn btn-light border">Submit</button>
    </form>

</div>

<!-- ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/js/bootstrap.bundle.min.js" integrity="sha512-VK2zcvntEufaimc+efOYi622VN5ZacdnufnmX7zIhCPmjhKnOi9ZDMtg1/ug5l183f19gG1/cBstPO4D8N/Img==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js" integrity="sha512-+H4iLjY3JsKiF2V6N366in5IQHj2uEsGV7Pp/GRcm0fn76aPAk5V8xB6n8fQhhSonTqTXs/klFz4D0GIn6Br9g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.43/moment-timezone.min.js" integrity="sha512-8jXWaevG4zfG+iTP8Lz7fNs3kyZyOrWaLf107FWPHa7/Jjzy7zmIMat3vbEdtLw35xtf6NtvHhRSFBdRHAWSnw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.43/moment-timezone-with-data-10-year-range.min.js" integrity="sha512-y2eYup+msVcQKIPgbC9stlGRwpZuVFBfSY6uavnpxL+Rz6iVGRSRzCMLFbed0EMKg6Fbq52TmVwN6CPuNHcHLQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    if (Intl.DateTimeFormat().resolvedOptions().timeZone != '<?= $timezone ?>') {
        document.cookie = 'timezone='
            + Intl.DateTimeFormat().resolvedOptions().timeZone
            + '; path=/';
        location.reload();
    }

    $("#datetime").select(); // or .focus() to not select contents

    if (!<?= $error ?>) {
        // Display formats for MomentJS: https://momentjs.com/docs/#/displaying/format/
        local_time = moment.unix(unix_timestamp).tz(moment.tz.guess());
        utc_time = moment.unix(unix_timestamp).tz('UTC');
        $("#timestamp_style_date").html('<?= $timestamp ?>');
        $("#gregorian_style_date").html(local_time.format('ddd ll LTS z (ZZ)'));
        $("#log_style_date").html(utc_time.format('yyyy-MM-DD HH:mm:ss z (ZZ)')
            + "<br/>" + local_time.format('yyyy-MM-DD HH:mm:ss z (ZZ)'));
        $("#duration_style_date").html(local_time.fromNow());
    }

    $("#input").submit(function(event){
        event.preventDefault();
        datetime_input = $("#datetime").val();
        window.location.replace("/" + datetime_input);
    });
</script>

</body>
</html>
